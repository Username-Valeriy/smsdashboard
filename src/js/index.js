import $ from 'jquery'
window.jQuery = $;
window.$ = $;

import OverlayScrollbars from 'overlayscrollbars/js/OverlayScrollbars';
/*
ON LOAD PAGE FUNCTION
*/

jQuery( window ).on( 'load', function() {

    $('body').removeClass('is-load');

} );

/*
INITIALIZATION FUNCTIONS
*/

jQuery( document ).ready( function( $ ) {

    OverlayScrollbars(document.querySelectorAll('.mess-container'), { });

    $(".burger-menu").on('click' ,function () {
        $(this).toggleClass("is-active");
        $('.header--content nav').toggleClass("is-active")
        $(".drop-down").removeClass('is-active')
        $('body').toggleClass("no-scroll")
    });
    $(".drop-down").on('click' , function () {
        $(this).toggleClass("is-active");

    });
    $('a').bind("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
    });
    $('.button-box span').on('click', function (){
        $(".popup-login").addClass('is-active');
        $('body').addClass("no-scroll")
    })
    $('.button-box a').on('click', function (){
        $('[type=email]').focus()
        $('.header--content nav').removeClass('is-active')
        $('.burger-menu').removeClass('is-active')
        $('body').removeClass("no-scroll")
    })
    return false;



} );

/*
ON SCROLL PAGE FUNCTIONS
*/

$(document).mouseup(function (e){
    var block = $(".popup-login form");
    if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        $(".popup-login").removeClass('is-active');
        $('body').removeClass("no-scroll")
    }
});